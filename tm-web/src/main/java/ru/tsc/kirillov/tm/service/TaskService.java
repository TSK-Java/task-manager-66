package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class TaskService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    public TaskDto create() {
        @NotNull final TaskDto task = new TaskDto(
                "Новая задача: " + System.currentTimeMillis(),
                "Описание",
                new Date(),
                new Date());
        save(task);
        return task;
    }

    public TaskDto save(@NotNull final TaskDto task) {
        return repository.save(task);
    }

    @Nullable
    public Collection<TaskDto> findAll() {
        return repository.findAll();
    }

    @Nullable
    public TaskDto findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

    public void remove(@NotNull final TaskDto task) {
        repository.delete(task);
    }

    public void remove(@NotNull final List<TaskDto> tasks) {
        tasks
                .stream()
                .forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return repository.existsById(id);
    }

    public void clear() {
        repository.deleteAll();
    }

    public long count() {
        return repository.count();
    }
    
}
