package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class ProjectService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    public ProjectDto create() {
        @NotNull final ProjectDto project = new ProjectDto(
                "Новый проект: " + System.currentTimeMillis(),
                "Описание",
                new Date(),
                new Date());
        save(project);
        return project;
    }

    public ProjectDto save(@NotNull final ProjectDto project) {
        return repository.save(project);
    }

    @Nullable
    public Collection<ProjectDto> findAll() {
        return repository.findAll();
    }

    @Nullable
    public ProjectDto findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

    public void remove(@NotNull final ProjectDto project) {
        repository.delete(project);
    }

    public void remove(@NotNull final List<ProjectDto> projects) {
        projects
                .stream()
                .forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return repository.existsById(id);
    }

    public void clear() {
        repository.deleteAll();
    }

    public long count() {
        return repository.count();
    }
    
}
