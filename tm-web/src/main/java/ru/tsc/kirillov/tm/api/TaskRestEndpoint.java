package ru.tsc.kirillov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.List;

@RequestMapping("/api/task")
public interface TaskRestEndpoint {

    @GetMapping("/findAll")
    List<TaskDto> findAll();

    @GetMapping("/findById/{id}")
    TaskDto findById(@NotNull @PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@NotNull @PathVariable("id") String id);

    @PostMapping("/save")
    TaskDto save(@NotNull @RequestBody TaskDto task);

    @PostMapping("/delete")
    void delete(@NotNull @RequestBody TaskDto task);

    @PostMapping("/deleteAll")
    void clear(@NotNull @RequestBody List<TaskDto> tasks);

    @PostMapping("/clear")
    void clear();

    @PostMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @GetMapping("/count")
    long count();
    
}
